#include<stdio.h>
#include<string.h>
int main(){
	
	int mat1[3][3]={{2,4,6},{1,3,5},{1,1,1}};//mattrix1 declaring
	int mat2[3][3]={{6,4,2},{5,3,1},{1,1,1}};//mattrix2 declaring
	int mat3[3][3];//mattrix3 declaring
	int mat4[3][3];//mattrix4 declaring
	
	for(int i=0;i<3;i++){//mattrix addition
		for(int k=0;k<3;k++){
			mat3[i][k]=mat1[i][k]+mat2[i][k];
		}
	}
	for(int i=0;i<3;i++){//mattrix multiplication
		for(int k=0;k<3;k++){
			mat4[i][k]=mat1[i][k]*mat2[i][k];
		}
	}

	for(int i=1;i<5;i++){ //mattrix display
		printf("Mattrix%i\n",i);
		if(i==3){
			printf("Addition of Mattrix1 and Mattrix2\n");
		}else if(i==4){
			printf("Multiplication of Mattrix1 and Mattrix2\n");
		}
		for(int m=0; m<3; m++) {
      		for(int j=0;j<3;j++) {
      			if(i==1){
      				printf("%d ", mat1[m][j]);
				}else if(i==2){
					printf("%d ", mat2[m][j]);
				}else if(i==3){
					printf("%d ", mat3[m][j]);
				}else{
					printf("%d ", mat4[m][j]);
				}
         		if(j==2){
            		printf("\n");
         		}
      		}
   		}
   		
   		printf("\n");
	}

	return 0;
}
