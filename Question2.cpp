#include<stdio.h>
#include<string.h>
int main(){
	
	char c[100];//declaring a character array
	int count = 0;//integer variable to count 
	char ch;//character variable ch
	
	printf("Enter the sentence: \n");
	scanf("%[^\n]s",c);//asking user to enter a sentence
	
	int k=strlen(c);//length of array c
	
	printf("Enter the letter that you want to count:\n");
	scanf(" %c",&ch);//asking user to enter the letter that he/she want to check
	
	for(int i=0;i<k;i++){//for loop to count how many ch characters included in the sentence that the user entered	
	
		if(c[i]== ch){
			
			count++;
			
		}	
		
	}
	
	printf("Count of %c is = %i",ch,count);//display ch letter count 
	
	return 0;
}
