#include<stdio.h>
#include<string.h>
int main(){
	
	char c[100];//declaring a character array
	char r[100];//declaring a character array
	printf("Enter the sentence: \n");
	scanf("%[^\n]s",c);//ask user to enter a sentence
	
	strcpy(r,c);//copying sentence to character array r
	
	int k = strlen(r); //length of array r
	printf("Reversed sentence:\n");
	for(int i=k-1;i>=0;i--){ //displaying array r reversed	
		
		if(r[i]== ' '){
			r[i]='\0';
			printf("%s ", &(r[i]) + 1);
		}	
	}
	printf("%s", r); //printing last word
	return 0;
}
